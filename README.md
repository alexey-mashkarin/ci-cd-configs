# CI-CD configs

Common gitlab CI/CD configs

## Example of project `.gitlab-ci.yml` file

```yaml
variables:
  PHP_UNIT_TEST_IMAGE: "registry.gitlab.com/alexey-mashkarin/nginx-php-fpm:php8.1"
  PHP_UNIT_TEST_RUN_POSTGRES: "true"
#  PHP_UNIT_TEST_POSTGRES_IMAGE: "postgres:14.2-alpine3.15"
#  PHP_UNIT_TEST_RUN_MARIADB: "true"
#  PHP_UNIT_TEST_MARIADB_IMAGE: "mariadb:10.7.3-focal"
#  PHP_UNIT_TEST_RUN_MONGO: "true"
#  PHP_UNIT_TEST_MARIADB_IMAGE: "mongo:5.0.6-focal"
#  BUILD_DISABLED: "true"
#  PHP_UNIT_TESTS_DISABLED: "true"
#  SAST_DISABLED: "true"
#  DEPLOY_DISABLED: "true"
#  CONTAINER_SCANNING_DISABLED: "true"
#  PRODUCTION_DEPLOY_DISABLED: "true"

include:
  - project: 'devops/ci-cd-configs'
    file: '/default.yml'
```